const items= require('./data.cjs');

function cb(item,index , arr){
    return item%2 === 0;
}

function filter(elements , cb){
    if(arguments < 2 || !Array.isArray(elements) ){
        return [];
    }
    let newArray =[];
    for (let index = 0; index < elements.length; index++){
        let output= cb(elements[index], index , elements);

        if(output === true){
            newArray.push(elements[index]);
        }
    }
    return  newArray;
}
console.log(filter(items,cb));

module.exports =filter;