const items = require('./data.cjs');


const nestedArray = [1, [2], [[3]], [[[4]]]]; 


function flatten(elements , depth=1){
  if( depth < 1){
    return elements;
  }
  let newArray=[];
  for( let index=0; index < elements.length; index++){
    if (Array.isArray(elements[index]) && (depth > 0 || depth === 'Infinity')) {
      newArray = newArray.concat(flatten(elements[index], depth - 1));
      } 
    else {
      if(elements[index]  || elements[index]===0)
        newArray.push(elements[index]);
    }
  }
    return newArray
}

console.log(flatten(nestedArray,'Infinity'));


module.exports = flatten;[1, , 3, ["a", , ["d", , "e"]]]