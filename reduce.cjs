const items= require('./data.cjs');

function cb(accumulator, item, index ,elements){
    return accumulator + item;
}
function reduce(elements, cb, initialValue) {
    let accumulator = initialValue === undefined ? elements[0] : initialValue;
    for (let index = initialValue === undefined ? 1 : 0; index < elements.length; index++) {
      accumulator = cb(accumulator, elements[index], index, elements);
    }
    return accumulator;
  }
  
// function reduce(elements,cb, startingValue){
    
//     // if(startingValue === undefined){
//     //     startingValue = elements[0];
//     // }
    
//     // for(let index=0; index < elements.length; index++){
//     //     startingValue = cb(startingValue,elements[index]);
        
//     // }
//     // return startingValue;
// }

console.log(reduce(items , cb, 0));

module.exports = reduce;