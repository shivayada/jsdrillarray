const items= require('./data.cjs');

function cb(item, value){
   if(item === value){
    return item;
   }
}
function find(elements , cb){
    if(arguments < 2 || !Array.isArray(elements) ){
        return undefined;
    }
    if(elements.length===0){
        return undefined;
    }
    for( let index=0; index < elements.length ; index++){
        if(cb(elements[index],2) ){
            return elements[index];
        }
    }

    return undefined;

    
}

console.log(find(items , cb));

module.exports = find;