const flatten  = require('../flatten.cjs');
const nestedArray= require('../nested.cjs')


test ("provide the new array" ,() =>{
    expect(flatten(nestedArray)).toStrictEqual([1, 2, 3, 4])
})