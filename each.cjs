const items= require('./data.cjs');

function cb(item, index){
    console.log(item);
}

function each(elements, cb){
    
    if(arguments < 2 || !Array.isArray(elements) ){
        return [];
    }
    let newArray=[];
    for (let index=0;index < elements.length ; index++){
        newArray.push(cb(elements[index], index, elements));
    }
    return newArray;

}
each(items,cb);

module.exports = each;
