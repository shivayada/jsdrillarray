const items= require('./data.cjs');

function cb(item , index , elements){
    return item*item;
}

function map(elements,cb){
    if(arguments.length < 2 || !Array.isArray(elements)){
        return [];
    }
    let newArray = [];
    for( let index = 0; index < elements.length ; index++ ){
        newArray[index] = cb(elements[index] , index , elements);
    }
   return newArray;

}

console.log(map(items,cb));

module.exports = map;